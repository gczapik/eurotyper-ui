/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vaadinexampleproject.restclient;

import com.vaadinexampleproject.samples.backend.data.Score;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Grzesiek
 */
public class JSonParserTest {

    @Test
    public void testParseOverallScore() 
    {
        //given
        String jsonResponse = "[{\"username\":\"miras\",\"points\":0.0,\"message\":null},{\"username\":\"rysio\",\"points\":2.0,\"message\":null}]";
        JSonParser parser = new JSonParser();
        
        //when
        List<Score> overallScore = parser.parseOverallScore(jsonResponse);
        
        //then
        assertEquals("miras", overallScore.get(0).getUsername());
        assertEquals(Double.valueOf(0), overallScore.get(0).getPoints());
        assertEquals("rysio", overallScore.get(1).getUsername());
        assertEquals(Double.valueOf(2), overallScore.get(1).getPoints());
    }
    
}
