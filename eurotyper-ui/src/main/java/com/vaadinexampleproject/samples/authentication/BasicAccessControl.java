package com.vaadinexampleproject.samples.authentication;

import com.vaadinexampleproject.restclient.Service;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default mock implementation of {@link AccessControl}. This implementation
 * accepts any string as a password, and considers the user "admin" as the only
 * administrator.
 */
public class BasicAccessControl implements AccessControl {

    @Override
    public boolean signIn(String username, String password) {
        try {
            if (username == null || username.isEmpty() || password == null || password.isEmpty())
                return false;
            
            if (Service.login(username, password))
            {
                CurrentUser.set(username);
                return true;
            }
            return false;
        } catch (IOException ex) {
            Logger.getLogger(BasicAccessControl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean isUserSignedIn() {
        return !CurrentUser.get().isEmpty();
    }

    @Override
    public boolean isUserInRole(String role) {
//        if ("admin".equals(role)) {
//            // Only the "admin" user is in the "admin" role
//            return getPrincipalName().equals("admin");
//        }

        // All users are in all non-admin roles
        return true;
    }

    @Override
    public String getPrincipalName() {
        return CurrentUser.get();
    }

}
