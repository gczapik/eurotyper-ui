package com.vaadinexampleproject.samples.authentication;

import com.vaadin.event.ShortcutAction;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadinexampleproject.restclient.Service;
import java.io.Serializable;

/**
 * UI content when the user is not logged in yet.
 */
public class LoginScreen extends CssLayout {

    private TextField username;
    private PasswordField password;
    private Button login;
    private Button forgotPassword;
    private LoginListener loginListener;
    private AccessControl accessControl;
    private TextField usernameReg;
    private PasswordField passwordReg;
    private TextField email;

    public LoginScreen(AccessControl accessControl, LoginListener loginListener) {
        this.loginListener = loginListener;
        this.accessControl = accessControl;
        buildUI();
        username.focus();
    }

    private void buildUI() {
        addStyleName("login-screen");

        // login form, centered in the available part of the screen
        Component loginForm = buildLoginForm();
        /*Component registerForm = */buildRegisterForm((FormLayout) loginForm);

        // layout to center login form when there is sufficient screen space
        // - see the theme for how this is made responsive for various screen
        // sizes
        VerticalLayout centeringLayout = new VerticalLayout();
        centeringLayout.setStyleName("centering-layout");
        centeringLayout.addComponent(loginForm);
//        centeringLayout.addComponent(registerForm);
        centeringLayout.setComponentAlignment(loginForm,
                Alignment.MIDDLE_CENTER);
//        centeringLayout.setComponentAlignment(registerForm,
//                Alignment.MIDDLE_CENTER);

        // information text about logging in
        CssLayout loginInformation = buildLoginInformation();

        addComponent(centeringLayout);
        addComponent(loginInformation);
    }

    private Component buildLoginForm() {
        FormLayout loginForm = new FormLayout();
//        VerticalLayout loginForm = new VerticalLayout();

        loginForm.addStyleName("login-form");
        loginForm.setSizeUndefined();
        loginForm.setMargin(false);

        loginForm.addComponent(username = new TextField("Username", ""));
        username.setWidth(15, Unit.EM);
        loginForm.addComponent(password = new PasswordField("Password"));
        password.setWidth(15, Unit.EM);
        password.setDescription("Write anything");
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setStyleName("buttons");
        buttons.setSpacing(true);
        loginForm.addComponent(buttons);

        buttons.addComponent(login = new Button("Login"));
        login.setDisableOnClick(true);
        login.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    login();
                } finally {
                    login.setEnabled(true);
                }
            }
        });
        login.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        login.addStyleName(ValoTheme.BUTTON_FRIENDLY);

        buttons.addComponent(forgotPassword = new Button("Forgot password?"));
        forgotPassword.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                showNotification(new Notification("It's so sad... :("));
            }
        });
        forgotPassword.addStyleName(ValoTheme.BUTTON_LINK);
        return loginForm;
    }
    
    private Component buildRegisterForm(FormLayout form) {
        form.addComponent(usernameReg = new TextField("Username", ""));
        usernameReg.setWidth(15, Unit.EM);
        form.addComponent(passwordReg = new PasswordField("Password"));
        passwordReg.setWidth(15, Unit.EM);
        passwordReg.setDescription("min 5 chars");
        form.addComponent(email = new TextField("Email", ""));
        email.setWidth(15, Unit.EM);
        email.setDescription("email to which you'll get the results");
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setStyleName("buttons");
        buttons.setSpacing(true);
        form.addComponent(buttons);

        Button register;
        buttons.addComponent(register = new Button("Register"));
        register.setDisableOnClick(true);
        register.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    String modifiedResponse = Service.addUser(usernameReg.getValue(), passwordReg.getValue(), email.getValue());
                    showNotification(new Notification(modifiedResponse));
                } finally {
                    register.setEnabled(true);
                }
            }
        });
        register.setDisableOnClick(true);
        register.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        return form;
    }

    private CssLayout buildLoginInformation() {
        CssLayout loginInformation = new CssLayout();
        loginInformation.setStyleName("login-information");
        Label loginInfoText = new Label(
                "<h1>Please read this</h1>"
                        + "This is the tool for internal Service Engine (members of others teams are welcome as well) Typers League for Euro 2016 matches. "
                        + "If you want to take part in this, please register here and pay a 20 PLN entry fee. You can give money to Piotr X Mirek or Grzegorz Czapik. "
                        + "If you don't know where to find us please ping us on IM/Jabber. <br/><br/>"
                        + "Collected money will be spent on some nice gift for the winner. <br/><br/>"
                        + "Have fun! :)",
                ContentMode.HTML);
        loginInformation.addComponent(loginInfoText);
        return loginInformation;
    }

    private void login() {
        if (accessControl.signIn(username.getValue(), password.getValue())) {
            loginListener.loginSuccessful();
        } else {
            showNotification(new Notification("Login failed",
                    "Please check your username and password and try again.",
                    Notification.Type.HUMANIZED_MESSAGE));
            username.focus();
        }
    }

    private void showNotification(Notification notification) {
        // keep the notification visible a little while after moving the
        // mouse, or until clicked
        notification.setDelayMsec(2000);
        notification.show(Page.getCurrent());
    }

    public interface LoginListener extends Serializable {
        void loginSuccessful();
    }
}
