package com.vaadinexampleproject.samples.crud;

import com.vaadin.server.Page;
import com.vaadinexampleproject.MyUI;
import com.vaadinexampleproject.restclient.Service;
import com.vaadinexampleproject.samples.authentication.CurrentUser;
import com.vaadinexampleproject.samples.backend.DataService;
import com.vaadinexampleproject.samples.backend.data.Match;
import com.vaadinexampleproject.samples.backend.data.Match;
import com.vaadinexampleproject.samples.backend.data.UserType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This class provides an interface for the logical operations between the CRUD
 * view, its parts like the product editor form and the data source, including
 * fetching and saving products.
 *
 * Having this separate from the view makes it easier to test various parts of
 * the system separately, and to e.g. provide alternative views for the same
 * data.
 */
public class SampleCrudLogic {

    private SampleCrudView view;

    public SampleCrudLogic(SampleCrudView simpleCrudView) {
        view = simpleCrudView;
    }

    public void init() {
        editProduct(null);
        // Hide and disable if not admin
        if (!MyUI.get().getAccessControl().isUserInRole("admin")) {
            view.setNewProductEnabled(false);
        }

//        view.showMatches(DataService.get().getAllProducts());
        view.showMatches(extractMatches(Service.getUserMatches(CurrentUser.get())));
        view.showUserScores(Service.getOverallScore());
    }

    public void cancelProduct() {
        setFragmentParameter("");
        view.clearSelection();
        view.editProduct(null);
    }

    /**
     * Update the fragment without causing navigator to change view
     */
    private void setFragmentParameter(String productId) {
        String fragmentParameter;
        if (productId == null || productId.isEmpty()) {
            fragmentParameter = "";
        } else {
            fragmentParameter = productId;
        }

        Page page = MyUI.get().getPage();
        page.setUriFragment("!" + SampleCrudView.VIEW_NAME + "/"
                + fragmentParameter, false);
    }

    public void enter(String productId) {
//        if (productId != null && !productId.isEmpty()) {
//            if (productId.equals("new")) {
//                newProduct();
//            } else {
//                // Ensure this is selected even if coming directly here from
//                // login
//                try {
//                    int pid = Integer.parseInt(productId);
//                    Match product = findProduct(pid);
//                    view.selectRow(product);
//                } catch (NumberFormatException e) {
//                }
//            }
//        }
    }

//    private Match findProduct(int productId) {
//        return DataService.get().getProductByHomeTeam(productId);
//    }

    public void saveProduct(Match match) {
        view.showSaveNotification(match.getHomeTeam() + " "
                +match.getResult()+" "+ match.getAwayTeam() + " saved");
        view.clearSelection();
        view.editProduct(null);
        view.refreshProduct(match);
        setFragmentParameter("");
    }

    public void editProduct(Match match) {
        if (match == null) {
            setFragmentParameter("");
        } else {
            setFragmentParameter(match.getHomeTeam() + "");
        }
        view.editProduct(match);
    }

    public void newProduct() {
        view.clearSelection();
        setFragmentParameter("new");
        view.editProduct(new Match());
    }

    public void rowSelected(Match product) {
        if (MyUI.get().getAccessControl().isUserInRole("admin")) {
            view.editProduct(product);
        }
    }

    private Collection<Match> extractMatches(List<UserType> userTypes) 
    {
        List<Match> matches = new ArrayList<>();
        for (UserType userType: userTypes){
            matches.add(userType.getMatch());
        }
        return matches;
    }
}
