/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vaadinexampleproject.samples.crud;

import com.vaadinexampleproject.samples.backend.data.Score;
import java.util.Comparator;

/**
 *
 * @author Grzesiek
 */
public class UserScoresComparator implements Comparator<Score>
{

    @Override
    public int compare(Score s1, Score s2) {
        int result = s1.getPoints().compareTo(s2.getPoints());
        if (result != 0){
            result=result*(-1); //descending
        }
        return result;
    }
    
}
