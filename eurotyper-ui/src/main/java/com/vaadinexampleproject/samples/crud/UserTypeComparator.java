/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vaadinexampleproject.samples.crud;

import com.vaadinexampleproject.samples.backend.data.UserType;
import java.util.Comparator;

/**
 *
 * @author Grzesiek
 */
public class UserTypeComparator implements Comparator<UserType>{

    @Override
    public int compare(UserType ut1, UserType ut2) {
        return ut1.getMatch().getTime().compareTo(ut2.getMatch().getTime());
    }
    
}
