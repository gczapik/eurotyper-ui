package com.vaadinexampleproject.samples.crud;

import com.vaadin.data.util.ObjectProperty;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid.SelectionModel;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadinexampleproject.restclient.JSonParser;
import com.vaadinexampleproject.restclient.Service;
import com.vaadinexampleproject.samples.authentication.CurrentUser;
import com.vaadinexampleproject.samples.backend.DataService;
import com.vaadinexampleproject.samples.backend.data.Match;
import com.vaadinexampleproject.samples.backend.data.Score;
import com.vaadinexampleproject.samples.backend.data.UserType;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class SampleCrudView extends CssLayout implements View {
    

    public static final String VIEW_NAME = "Your dashboard";
    private MatchesGrid grid;
    private ScoresGrid playerScoresGrid;
    private ProductForm form;

    private SampleCrudLogic viewLogic = new SampleCrudLogic(this);
    private Button newProduct;
    
    private List<UserType> userTypes;

    public SampleCrudView() {
        setSizeFull();
        Panel parentPanel = new Panel();
        parentPanel.setSizeFull();
        addStyleName("crud-view");

        grid = new MatchesGrid();
        
        playerScoresGrid = new ScoresGrid(); 

        form = new ProductForm(viewLogic);
        form.setCategories(DataService.get().getAllCategories());

        HorizontalLayout twoGridsLayout = new HorizontalLayout();
        twoGridsLayout.setSpacing(true);
        VerticalLayout matchScoresLayout = new VerticalLayout();
        matchScoresLayout.addComponent(createTopBar("Match results:"));
        matchScoresLayout.addComponent(grid);
        matchScoresLayout.setMargin(true);
        matchScoresLayout.setSpacing(true);
        matchScoresLayout.setSizeFull();
        matchScoresLayout.setExpandRatio(grid, 1);
        matchScoresLayout.setStyleName("crud-main-layout");
        
        VerticalLayout userScoresLayout = new VerticalLayout();
        userScoresLayout.addComponent(createTopBar("Current users ranking:"));
        userScoresLayout.addComponent(playerScoresGrid);
        userScoresLayout.setMargin(true);
        userScoresLayout.setSpacing(true);
        userScoresLayout.setSizeFull();
        userScoresLayout.setExpandRatio(playerScoresGrid, 1);
        userScoresLayout.setStyleName("crud-main-layout");

        twoGridsLayout.addComponents(matchScoresLayout, userScoresLayout);
        VerticalLayout gridsAndMatches = new VerticalLayout();
        gridsAndMatches.addComponent(twoGridsLayout);
        gridsAndMatches.addComponent(createTypesForm());
        parentPanel.setContent(gridsAndMatches);
        addComponent(parentPanel);
        addComponent(form);

        parentPanel.getContent().setSizeUndefined();
        viewLogic.init();
    }

    public HorizontalLayout createTopBar(String text) {
        Label l = new Label(text);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.setSpacing(true);
        topLayout.setWidth("100%");
        topLayout.addComponent(l);
        topLayout.setStyleName("top-bar");
        return topLayout;
    }

    @Override
    public void enter(ViewChangeEvent event) {
        viewLogic.enter(event.getParameters());
    }

    public void showError(String msg) {
        Notification.show(msg, Type.ERROR_MESSAGE);
    }

    public void showSaveNotification(String msg) {
        Notification.show(msg, Type.TRAY_NOTIFICATION);
    }

    public void setNewProductEnabled(boolean enabled) {
        newProduct.setEnabled(enabled);
    }

    public void clearSelection() {
        grid.getSelectionModel().reset();
    }

    public void editProduct(Match match) {
        if (match != null) {
            form.addStyleName("visible");
            form.setEnabled(true);
        } else {
            form.removeStyleName("visible");
            form.setEnabled(false);
        }
        form.editProduct(match);
    }

    public void showMatches(Collection<Match> matches) {
        grid.setProducts(matches);
    }

    public void showUserScores(Collection<Score> userScores) {
        playerScoresGrid.setScores(userScores);
    }
    
    public void refreshProduct(Match match) {
        grid.refresh(match);
        grid.scrollTo(match);
    }

    public void removeProduct(Match match) {
        grid.remove(match);
    }

    private Component createTypesForm() {
        userTypes = Service.getUserMatches(CurrentUser.get());
        VerticalLayout verticalLayoutForInputs = new VerticalLayout();
        verticalLayoutForInputs.addComponent(new Label("Put your types below"));
        verticalLayoutForInputs.setSpacing(true);
        for (UserType userType: userTypes)
        {
            HorizontalLayout layout = new HorizontalLayout();
            verticalLayoutForInputs.addComponent(layout); 
            createTypingRow(layout, userType);
        }
//        Component submitButton = createSubmitButton();
//        verticalLayoutForInputs.addComponent(submitButton);
        return verticalLayoutForInputs;
    }
    
    private Component createSingleSubmitButton()
    {
        Button singleSubmit = new Button("Submit this type");
        singleSubmit.addStyleName(ValoTheme.BUTTON_PRIMARY);
        singleSubmit.setIcon(FontAwesome.PLUS_CIRCLE);
        singleSubmit.setDisableOnClick(true);
        singleSubmit.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    String response = Service.submitUserType(
                            CurrentUser.get(), 
                            getDateStringForButton(event.getButton()), 
                            getHomeTeamForButton(event.getButton()), 
                            getAwayTeamForButton(event.getButton()), 
                            getScoreForButton(event.getButton()));
                    showNotification(new Notification(response));
                } finally {
                    singleSubmit.setEnabled(true);
                }
            }
        });
        return singleSubmit;
    }
    
    private String getDateStringForButton(Button clickedButton)
    {
        Label dateLabel = (Label) ((HorizontalLayout)clickedButton.getParent()).getComponent(0);
        return dateLabel.getCaption();
    }
    
    private String getHomeTeamForButton(Button clickedButton) {
        Label matchLabel = (Label) ((HorizontalLayout) clickedButton.getParent()).getComponent(1);
        return matchLabel.getCaption().split("-")[0].replaceAll(" ", "");
    }
    
    private String getAwayTeamForButton(Button clickedButton) {
        Label matchLabel = (Label) ((HorizontalLayout) clickedButton.getParent()).getComponent(1);
        return matchLabel.getCaption().split("-")[1].replaceAll(" ", "");
    }
    
    private String getScoreForButton(Button clickedButton) {
        TextField homeScore = (TextField) ((HorizontalLayout) clickedButton.getParent()).getComponent(2);
        TextField awayScore = (TextField) ((HorizontalLayout) clickedButton.getParent()).getComponent(4);
        return homeScore.getValue()+":"+awayScore.getValue();
    }

    private Component createSubmitButton() {
        Button submit = new Button("Submit your types!");
        submit.addStyleName(ValoTheme.BUTTON_PRIMARY);
        submit.setIcon(FontAwesome.PLUS_CIRCLE);
        submit.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event){
                try {
                    ((Layout)submit.getParent()).addComponent(new Label("response: "+Service.callRest()));
                } catch (IOException ex) {
                    Logger.getLogger(SampleCrudView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        return submit;
    }
    
    private void showNotification(Notification notification) {
        // keep the notification visible a little while after moving the
        // mouse, or until clicked
        notification.setDelayMsec(2000);
        notification.show(Page.getCurrent());
    }

    private void createTypingRow(HorizontalLayout layout, UserType userType) {
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        ObjectProperty<Integer> homeScoreProperty = new ObjectProperty<>(0); //fake value, removed in next line
        homeScoreProperty.setValue(userType.getHomeTeamScore());
        TextField homeTextField = new TextField();
        homeTextField.setImmediate(true);
        homeTextField.setNullRepresentation("");
        homeTextField.setNullSettingAllowed(true);
        homeTextField.setPropertyDataSource(homeScoreProperty);
        homeTextField.setInvalidAllowed(false);
        homeTextField.setWidth(3, Unit.EM);
        ObjectProperty<Integer> awayScoreProperty = new ObjectProperty<>(0); //fake value, removed in next line
        awayScoreProperty.setValue(userType.getAwayTeamScore());
        TextField awayTextField = new TextField();
        awayTextField.setImmediate(true);
        awayTextField.setNullRepresentation("");
        awayTextField.setNullSettingAllowed(true);
        awayTextField.setPropertyDataSource(awayScoreProperty);
        awayTextField.setInvalidAllowed(false);
        awayTextField.setWidth(3, Unit.EM);
        Label matchTime = new Label();
        matchTime.setCaption(format.format(userType.getMatch().getTime()));
        Label matchLabel = new Label();
        matchLabel.setCaption(userType.getMatch().getHomeTeam() + " - " + userType.getMatch().getAwayTeam());
        matchLabel.setWidth(15, Unit.EM);
        Date now = new Date();
        Component buttonOrLabel = null;
        if (!userType.getMatch().getResult().isEmpty()){
            homeTextField.setEnabled(false);
            awayTextField.setEnabled(false);
            buttonOrLabel = new Label("Match result: "+userType.getMatch().getResult());
        }
        else if (now.after(userType.getMatch().getTime())){
            homeTextField.setEnabled(false);
            awayTextField.setEnabled(false);
            buttonOrLabel = new Label("Match already started");
        } else 
        {
            buttonOrLabel = createSingleSubmitButton();
        }
        layout.addComponents(
                matchTime,
                matchLabel,
                homeTextField,
                new Label("-"),
                awayTextField, 
                buttonOrLabel);
        layout.setSpacing(true);
    }

}
