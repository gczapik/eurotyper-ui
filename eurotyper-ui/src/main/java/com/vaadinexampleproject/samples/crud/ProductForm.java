package com.vaadinexampleproject.samples.crud;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitEvent;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.fieldgroup.FieldGroup.CommitHandler;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.Page;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadinexampleproject.samples.backend.DataService;
import com.vaadinexampleproject.samples.backend.data.Availability;
import com.vaadinexampleproject.samples.backend.data.Category;
import com.vaadinexampleproject.samples.backend.data.Match;
import com.vaadinexampleproject.samples.backend.data.Match;
import java.util.Collection;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class ProductForm extends ProductFormDesign {

    private SampleCrudLogic viewLogic;
    private BeanFieldGroup<Match> fieldGroup;

    public ProductForm(SampleCrudLogic sampleCrudLogic) {
        super();
        addStyleName("product-form");
        viewLogic = sampleCrudLogic;

//        price.setConverter(new EuroConverter());

//        for (Availability s : Availability.values()) {
//            availability.addItem(s);
//        }

        fieldGroup = new BeanFieldGroup<Match>(Match.class);
        fieldGroup.bindMemberFields(this);

        // perform validation and enable/disable buttons while editing
//        ValueChangeListener valueListener = new ValueChangeListener() {
//            @Override
//            public void valueChange(ValueChangeEvent event) {
//                formHasChanged();
//            }
//        };
//        for (Field f : fieldGroup.getFields()) {
//            f.addValueChangeListener(valueListener);
//        }

//        fieldGroup.addCommitHandler(new CommitHandler() {
//
//            @Override
//            public void preCommit(CommitEvent commitEvent)
//                    throws CommitException {
//            }
//
//            @Override
//            public void postCommit(CommitEvent commitEvent)
//                    throws CommitException {
//                DataService.get().updateProduct(
//                        fieldGroup.getItemDataSource().getBean());
//            }
//        });

        save.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                try {
                    fieldGroup.commit();

                    // only if validation succeeds
                    Match product = fieldGroup.getItemDataSource().getBean();
                    viewLogic.saveProduct(product);
                } catch (CommitException e) {
                    Notification n = new Notification(
                            "Please re-check the fields", Type.ERROR_MESSAGE);
                    n.setDelayMsec(500);
                    n.show(getUI().getPage());
                }
            }
        });

        cancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                viewLogic.cancelProduct();
            }
        });

    }

    public void setCategories(Collection<Category> categories) {
        category.setOptions(categories);
    }

    public void editProduct(Match match) {
        if (match == null) {
            match = new Match();
        }
        fieldGroup.setItemDataSource(new BeanItem<Match>(match));

        // before the user makes any changes, disable validation error indicator
        // of the product name field (which may be empty)
        productName.setValidationVisible(false);

        // Scroll to the top
        // As this is not a Panel, using JavaScript
        String scrollScript = "window.document.getElementById('" + getId()
                + "').scrollTop = 0;";
        Page.getCurrent().getJavaScript().execute(scrollScript);
    }

    private void formHasChanged() {
        // show validation errors after the user has changed something
        productName.setValidationVisible(true);

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<Match> item = fieldGroup.getItemDataSource();
        if (item != null) {
            Match product = item.getBean();
//            canRemoveProduct = product.getId() != -1;
        }
        delete.setEnabled(canRemoveProduct);
    }
}
