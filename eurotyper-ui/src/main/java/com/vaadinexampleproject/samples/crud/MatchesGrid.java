package com.vaadinexampleproject.samples.crud;

import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.MethodProperty;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.converter.StringToEnumConverter;
import com.vaadin.data.util.converter.StringToIntegerConverter;
import com.vaadin.data.util.filter.Or;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadinexampleproject.samples.backend.data.Availability;
import com.vaadinexampleproject.samples.backend.data.Match;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class MatchesGrid extends Grid {

    public MatchesGrid() {
        setSizeFull();

        setSelectionMode(SelectionMode.SINGLE);

        BeanItemContainer<Match> container = new BeanItemContainer<Match>(
                Match.class);
        setContainerDataSource(container);
        setColumnOrder("homeTeam", "result", "awayTeam");
        setLocale(Locale.ENGLISH);
    }

    private BeanItemContainer<Match> getContainer() {
        return (BeanItemContainer<Match>) super.getContainerDataSource();
    }

    public void setProducts(Collection<Match> matches) {
        getContainer().removeAllItems();
        getContainer().addAll(matches);
    }

    public void refresh(Match match) {
        // We avoid updating the whole table through the backend here so we can
        // get a partial update for the grid
        BeanItem<Match> item = getContainer().getItem(match);
        if (item != null) {
            // Updated product
            MethodProperty p = (MethodProperty) item.getItemProperty("result");
            p.fireValueChange();
        } else {
            // New product
            getContainer().addBean(match);
        }
    }

    public void remove(Match match) {
        getContainer().removeItem(match);
    }
}
