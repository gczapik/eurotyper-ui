/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vaadinexampleproject.samples.crud;

import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.MethodProperty;
import com.vaadin.ui.Grid;
import com.vaadinexampleproject.samples.backend.data.Score;
import java.util.Collection;

public class ScoresGrid extends Grid {

    public ScoresGrid() {
        setSizeFull();

        setSelectionMode(Grid.SelectionMode.SINGLE);

        BeanItemContainer<Score> container = new BeanItemContainer<Score>(
                Score.class);
        setContainerDataSource(container);
        setColumnOrder("username", "points");
    }

    private BeanItemContainer<Score> getContainer() {
        return (BeanItemContainer<Score>) super.getContainerDataSource();
    }

    public void setScores(Collection<Score> scores) {
        getContainer().removeAllItems();
        getContainer().addAll(scores);
    }

    public void refresh(Score score) {
        // We avoid updating the whole table through the backend here so we can
        // get a partial update for the grid
        BeanItem<Score> item = getContainer().getItem(score);
        if (item != null) {
            // Updated product
            MethodProperty p = (MethodProperty) item.getItemProperty("result");
            p.fireValueChange();
        } else {
            // New product
            getContainer().addBean(score);
        }
    }

    public void remove(Score score) {
        getContainer().removeItem(score);
    }
}
