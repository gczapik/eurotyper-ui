/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vaadinexampleproject.restclient;

import com.vaadinexampleproject.samples.backend.data.Match;
import com.vaadinexampleproject.samples.backend.data.Score;
import com.vaadinexampleproject.samples.backend.data.Team;
import com.vaadinexampleproject.samples.backend.data.UserType;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONArray;
import org.json.JSONObject;


/**
 *
 * @author Grzesiek
 */
public class JSonParser 
{
    public List<UserType> parseGetAllMatchesResponse(String response)
    {
        List<UserType> userTypes = new ArrayList<>();
        JSONObject jsonResponse = new JSONObject(response);
        JSONArray matchScores = jsonResponse.getJSONArray("matchScore");
        Iterator<Object> it = matchScores.iterator();
        while(it.hasNext()){
            JSONObject jsonMatchAndType = (JSONObject) it.next();
            UserType userType = createUserType(jsonMatchAndType);
            userTypes.add(userType);
        }
        return userTypes;
    }
    
    public List<Score> parseOverallScore(String response) {
        List<Score> userScores = new ArrayList<>();
        JSONArray jsonUserScores = new JSONArray(response);
        Iterator<Object> it = jsonUserScores.iterator();
        while (it.hasNext()) {
            JSONObject jsonUserScore = (JSONObject) it.next();
            Score userScore = new Score();
            userScore.setUsername(jsonUserScore.getString("username"));
            if (!jsonUserScore.isNull("points")){
                userScore.setPoints(jsonUserScore.getDouble("points"));
            } else {
                userScore.setPoints(Double.valueOf(0));
            }
            userScores.add(userScore);
        }
        return userScores;
    }
    
    private Pair<Integer,Integer> parseMatchScore(String score)
    {
        score=score.replaceAll(" ", "");
        return new ImmutablePair<Integer,Integer>(Integer.valueOf(score.split(":")[0]), Integer.valueOf(score.split(":")[1]));
    }

    private Match createMatch(JSONObject jsonMatch) {
        Match match = new Match();
        Team home = new Team();
        match.setHomeTeam(home);
        home.setName(jsonMatch.getString("country1"));
        Team away = new Team();
        match.setAwayTeam(away);
        away.setName(jsonMatch.getString("country2"));
        Date matchTime = new Date(jsonMatch.getLong("time"));
        match.setTime(matchTime);
        if (!jsonMatch.isNull("score")){
            String jsonScore = jsonMatch.getString("score");
            Pair<Integer, Integer> score = parseMatchScore(jsonScore);
            match.getHomeTeam().setGoals(score.getLeft());
            match.getAwayTeam().setGoals(score.getRight());
        }
        return match;
    }

    private UserType createUserType(JSONObject jsonMatchAndType) {
        UserType userType = new UserType();
        userType.setMatch(createMatch(jsonMatchAndType.getJSONObject("match")));
        if (!jsonMatchAndType.isNull("type")){
            Pair<Integer, Integer> score = parseMatchScore(jsonMatchAndType.getString("type"));
            userType.setHomeTeamScore(score.getLeft());
            userType.setAwayTeamScore(score.getRight());
        }
        return userType;
    }
}
