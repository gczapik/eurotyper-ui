package com.vaadinexampleproject.restclient;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.vaadinexampleproject.samples.backend.data.Score;
import com.vaadinexampleproject.samples.backend.data.UserType;
import com.vaadinexampleproject.samples.crud.UserScoresComparator;
import com.vaadinexampleproject.samples.crud.UserTypeComparator;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;



public class Service {
    
private static final String BASE_URI = "http://localhost:8080/service/";
private static JSonParser jsonParser = new JSonParser();

 public static String callRest() throws IOException {
  ClientConfig config = new DefaultClientConfig();
  Client client = Client.create(config);
  WebResource service = client.resource(UriBuilder.fromUri(BASE_URI).build());
  
  return service.path("addmatch")
          .queryParam("date", "22.06.2016_21:22")
          .queryParam("country1", "POLAND")
          .queryParam("country2", "GERMANY")
          //.accept(MediaType.TEXT_HTML/*APPLICATION_JSON*/)
          .get(String.class);
  
   }
 
    public static boolean login(String username, String password) throws IOException {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri(BASE_URI).build());

        String response = service.path("login")
                .queryParam("username", username)
                .queryParam("password", password)
//                .accept(MediaType.TEXT_HTML/*APPLICATION_JSON*/)
                .get(String.class);
        return response.equalsIgnoreCase("Log In");
    }

    public static String addUser(String username, String password, String email) {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri(BASE_URI).build());

    String response = service.path("addUser")
            .queryParam("username", username)
            .queryParam("password", password)
            .queryParam("mail", email)
            //                .accept(MediaType.TEXT_HTML/*APPLICATION_JSON*/)
            .get(String.class);
    if (response.equalsIgnoreCase("User added successfully")){
        response+=". You can log in now.";
    }
    return response;
    }
    
    public static List<UserType> getUserMatches(String username) {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri(BASE_URI).build());

        String response = service.path("getUserMatches")
                .queryParam("username", username)
//                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);
        
        List<UserType> userTypes = jsonParser.parseGetAllMatchesResponse(response);
        Collections.sort(userTypes, new UserTypeComparator());
        return userTypes;
    }
    
    public static String submitUserType(String username, String date, String homeTeam, String awayTeam, String score) {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri(BASE_URI).build());
        DateFormat format = new SimpleDateFormat("dd.MM", Locale.US);

        return service.path("type")
                .queryParam("username", username)
                .queryParam("date", date.split("\\.")[0]+"."+date.split("\\.")[1])
                .queryParam("country1", homeTeam)
                .queryParam("country2", awayTeam)
                .queryParam("score", score)
                //                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);
    }
    
    public static List<Score> getOverallScore() {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri(BASE_URI).build());

        String response = service.path("printoverallscore")
                //                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);

        List<Score> userScores = jsonParser.parseOverallScore(response);
        Collections.sort(userScores, new UserScoresComparator());
        return userScores;
    }
    
}
