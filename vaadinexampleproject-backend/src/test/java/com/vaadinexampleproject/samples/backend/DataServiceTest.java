package com.vaadinexampleproject.samples.backend;

import org.junit.Before;
import org.junit.Test;
import com.vaadinexampleproject.samples.backend.data.Match;
import com.vaadinexampleproject.samples.backend.mock.MockDataService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Simple unit test for the back-end data service.
 */
public class DataServiceTest {

    private DataService service;

    @Before
    public void setUp() throws Exception {
        service = MockDataService.getInstance();
    }

//    @Test
//    public void testDataServiceCanFetchProducts() throws Exception {
//        assertFalse(service.getAllProducts().isEmpty());
//    }

//    @Test
//    public void testDataServiceCanFetchCategories() throws Exception {
//        assertFalse(service.getAllCategories().isEmpty());
//    }

//    @Test
//    public void testUpdateProduct_updatesTheProduct() throws Exception {
//        Match p = service.getAllProducts().iterator().next();
//        p.setProductName("My Test Name");
//        service.updateProduct(p);
//        Match p2 = service.getAllProducts().iterator().next();
//        assertEquals("My Test Name", p2.getProductName());
//    }
}
