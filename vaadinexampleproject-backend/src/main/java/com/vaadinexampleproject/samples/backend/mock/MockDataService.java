package com.vaadinexampleproject.samples.backend.mock;

import com.vaadinexampleproject.samples.backend.DataService;
import com.vaadinexampleproject.samples.backend.data.Category;
import com.vaadinexampleproject.samples.backend.data.Match;
import java.util.ArrayList;
import java.util.List;

/**
 * Mock data model. This implementation has very simplistic locking and does not
 * notify users of modifications.
 */
public class MockDataService extends DataService {

    private static MockDataService INSTANCE;

    private List<Match> matches;
    private List<Category> categories;
    private int nextProductId = 0;

    private MockDataService() {
        categories = MockDataGenerator.createCategories();
        matches = new ArrayList<>(); //gcz TODO - tu skonczylem -> wypelnij kolekcje meczami z RESTowego requesta (wszystkie mecze usera -> to co Piotrek mial dopisac)
        matches.addAll(MockDataGenerator.createProducts(null));
        nextProductId = matches.size() + 1;
    }

    public synchronized static DataService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MockDataService();
        }
        return INSTANCE;
    }

    @Override
    public synchronized List<Match> getAllProducts() {
        return matches;
    }

    @Override
    public synchronized List<Category> getAllCategories() {
        return categories;
    }

//    @Override
    public synchronized void updateProduct(Match p) {
//        if (p.getId() < 0) {
//            // New product
//            p.setId(nextProductId++);
//            products.add(p);
//            return;
//        }
//        for (int i = 0; i < products.size(); i++) {
//            if (products.get(i).getId() == p.getId()) {
//                products.set(i, p);
//                return;
//            }
//        }
//
//        throw new IllegalArgumentException("No product with id " + p.getId()
//                + " found");
    }

    @Override
    public synchronized Match getProductByHomeTeam(String homeTeam) { //TODO gcz - tu skonczylem -> zastanowic sie po czym wyciagac mecze
        for (int i = 0; i < matches.size(); i++) {
            if (matches.get(i).getHomeTeam().equals(homeTeam)) {
                return matches.get(i);
            }
        }
        return null;
    }

//    @Override
//    public synchronized void deleteProduct(int productId) {
//        Match p = getProductByHomeTeam(productId);
//        if (p == null) {
//            throw new IllegalArgumentException("Product with id " + productId
//                    + " not found");
//        }
//        matches.remove(p);
//    }
}
