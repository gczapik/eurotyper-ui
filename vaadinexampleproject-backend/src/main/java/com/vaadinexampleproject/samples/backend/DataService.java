package com.vaadinexampleproject.samples.backend;

import java.util.Collection;

import com.vaadinexampleproject.samples.backend.data.Category;
import com.vaadinexampleproject.samples.backend.data.Match;
import com.vaadinexampleproject.samples.backend.mock.MockDataService;

/**
 * Back-end service interface for retrieving and updating product data.
 */
public abstract class DataService {

    public abstract Collection<Match> getAllProducts();

    public abstract Collection<Category> getAllCategories();

//    public abstract void updateProduct(Match p);

//    public abstract void deleteProduct(int productId);

    public abstract Match getProductByHomeTeam(String homeTeam);

    public static DataService get() {
        return MockDataService.getInstance();
    }

}
