/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vaadinexampleproject.samples.backend.data;

import java.util.Date;

/**
 *
 * @author Grzesiek
 */
public class Match 
{
    private Team homeTeam;
    private Team awayTeam;
    private Date time;

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
    
    public String getResult()
    {
        if (homeTeam.getGoals() == null){
            return "";
        }
        String result = homeTeam.getGoals()+":"+awayTeam.getGoals();
        if (homeTeam.getPenaltyGoals() != null && awayTeam.getPenaltyGoals() != null){
            result+="p:("+homeTeam.getPenaltyGoals()+":"+awayTeam.getPenaltyGoals()+")";
        }
        return result;
    }

}
